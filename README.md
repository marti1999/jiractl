# jiractl tools

## Features

- duplicate a GitHub/GitLab issue as a JIRA issue

## Configuration

Configuration is stored in `~/.config/jiractl.yaml`

In the configuration file you can have:
- multiple GitHub or GitLab source projects
  (the first one is the default one)
- each source project has only one JIRA destination project

Example of configuration file:

```yaml
github:
    token: <your_github_token>
gitlab:
    token: <your_gitlab_token>
jira:
    server: https://jira.atlassian.com
    user: <your_jira_username>
    pass: <your_jira_password>
projects:
    - sardana:
        platform: gitlab
        repo: sardana-org/sardana
        jira:
            project: CSGSW
            issue_type: Story
            component: Sardana
            customer_unit: World
            transition: Planned    
    - pyicepap:
        platfomr: github
        repo: ALBA-Synchrotron/pyIcePAP
        jira:
            project: CS
            issue_type: Problem
            component: Motion
            customer_unit: Beamlines
            transition: Plan        
```

Hints:
- Issue transition names may directly map to the issue states when transitions
  are not explicitly configured. For example, in one project the _Plan_
  transition is explicitly defined and changes an issue to the _Planned_
  state, and in another project, this transition is not explicitly defined,
  hence the transition inherits its name from the _Planned_ state.
  Always consult the particular workflow configuration to obtain this
  information. 
  
## Usage

**IMPORTANT: To interface with ALBA's JIRA you need to be connected to the ALBA network e.g. using VPN.**

Assuming the above configuration file:

Create JIRA issue from sardana-org/sardana#1513 issue (GitLab):
```console
$> jiractl dup #1513
Issue URL: https://jira.atlassian.com/browse/CSGSW-14948
```

Create JIRA issue from sardana-org/sardana!1514 merge request (GitLab):
```console
$> jiractl dup !1514
Issue URL: https://jira.atlassian.com/browse/CSGSW-14948
```

Create JIRA issue from ALBA-Synchrotron/pyIcePAP#10 (GitHub) issue or PR:
```console
$> jiractl dup 10 --project=pyicepap
Issue URL: https://jira.atlassian.com/browse/CS-4100
```

## TODO:
- Add OAuth support for JIRA: https://jira.readthedocs.io/en/master/examples.html#oauth
- Add support to other fields: Assignee, etc.
