import click
from jiractl.jira import Jira
from jiractl.config import Config


@click.group('jiractl')
def jiractl():
    pass


@jiractl.command()
@click.option('--project',
              type=click.STRING,
              help='source project from the configuration file')
@click.argument('nb', type=click.STRING)
def dup(nb, project):
    """Import issue from a GitHub project to a JIRA project.

    Projects information must be configured in ~/.config/jiractl.yaml
    """
    config = Config()
    project = config.get_project(project)
    if project.platform == "github":
        from jiractl.github import Github
        platform = Github(token=config.github_token)
    elif project.platform == "gitlab":
        from jiractl.gitlab import Gitlab
        platform = Gitlab(token=config.gitlab_token)
    else:
        raise ValueError('Unknown platform "{}"'.format(project.platform))
    issue_dict = platform.get_item_info(project.repo, nb)
    jira = Jira(server=config.jira_server,
                credentials=config.jira_credentials)
    jira_project = project.jira_project
    jira_project_fields = Jira.get_issue_fields(jira_project)
    issue_dict.update(jira_project_fields)
    issue = jira.create_issue(issue_dict)
    print('Issue URL: {}'.format(issue.permalink()))

    transition = jira_project.transition
    if transition is not None:
        jira.transition_issue(issue, transition)
