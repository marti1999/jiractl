import yaml
from pathlib import Path


class Config:

    def __init__(self, path=None):
        """Read configuration from YAML file"""
        if path is None:
            path = Path.home() / ".config/jiractl.yaml"
        with open(path, "r") as yaml_file:
            self._config = yaml.load(yaml_file, Loader=yaml.FullLoader)

    def get_project(self, project=None):
        for project_info in self._config["projects"]:
            name = list(project_info)[0]
            if project is None or name == project:
                return Project(project_info[name])

    @property
    def jira_server(self):
        return self._config['jira']['server']

    @property
    def jira_credentials(self):
        return self._config['jira']['user'], self._config['jira'].get('pass', None)

    @property
    def github_token(self):
        return self._config['github']['token']
    
    @property
    def gitlab_token(self):
        return self._config['gitlab']['token']


class Project:

    def __init__(self, config):
        self._config = config

    @property
    def repo(self):
        return self._config['repo']
    
    @property
    def platform(self):
        return self._config['platform']

    @property
    def jira_project(self):
        return JiraProject(self._config['jira'])


class JiraProject:

    def __init__(self, config):
        self._config = config

    @property
    def project(self):
        return self._config['project']

    @property
    def issue_type(self):
        return self._config['issue_type']

    @property
    def component(self):
        return self._config['component']

    @property
    def customer_unit(self):
        return self._config['customer_unit']

    @property
    def transition(self):
        return self._config.get('transition', None)
