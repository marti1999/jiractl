from github import Github as Github_


class Github:

    def __init__(self, token):
        self.github = Github_(token)

    def get_item_info(self, repo, nb):
        """Get issue/PR information from GitHub"""
        repo = self.github.get_repo(repo)
        try:
            issue_or_pr = repo.get_issue(nb)
        except Exception as e:
            raise ValueError(
                "issue or pr with {} could not be retrieved".format(nb)) from e
        issue_dict = {
            'summary': issue_or_pr.title,
            'description': issue_or_pr.html_url,
        }
        return issue_dict
