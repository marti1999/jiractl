from gitlab import Gitlab as Gitlab_


class Gitlab:

    MR_PREFIX = "!"
    ISSUE_PREFIX = "#" 

    def __init__(self, token):
        self.gitlab = Gitlab_(url="https://gitlab.com", private_token=token)

    def get_item_info(self, repo, nb):
        """Get issue/PR information from GitLab"""
        item_prefix = nb[0]
        nb = nb[1:]
        assert item_prefix in (Gitlab.MR_PREFIX, Gitlab.ISSUE_PREFIX), \
            "GitLab numbers must start with '!' or '#'"
        repo = self.gitlab.projects.get(repo)
        if item_prefix == Gitlab.ISSUE_PREFIX:
            try:
                item = repo.issues.get(nb)
            except Exception as e:
                raise ValueError(
                        "issue with id {} could not be retrieved".format(nb)) from e
        elif item_prefix == Gitlab.MR_PREFIX:
            try:
                item = repo.mergerequests.get(nb)
            except Exception as e:
                raise ValueError(
                    "mr with id {} could not be retrieved".format(nb)) from e
        issue_dict = {
            'summary': item.title,
            'description': item.web_url,
        }
        return issue_dict
