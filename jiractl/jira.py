from jira import JIRA


custom_fields_map = {
    'customer_unit': 'customfield_10204'
}


class Jira:

    def __init__(self, server, credentials):
        username = credentials[0]
        password = credentials[1]
        if password is None:
            from getpass import getpass

            password = getpass("Jira password for {}?: ".format(username))
        self.jira = JIRA(server, basic_auth=(username, password))

    def create_issue(self, issue_dict):
        """Create issue from a dict with the issue fields"""
        return self.jira.create_issue(fields=issue_dict)

    def transition_issue(self, issue, transition):
        """Transition issue to a different state e.g. Plan, Resolve"""
        self.jira.transition_issue(issue, transition)

    @staticmethod
    def get_issue_fields(project):
        """Get dict with issue fields from project information"""
        customer_unit = custom_fields_map['customer_unit']
        issue_dict = {
            'project': {'key': project.project},
            'issuetype': {'name': project.issue_type},
            'components': [{'name': project.component}],
            customer_unit: {'value': project.customer_unit},
        }
        return issue_dict
